
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

 var ehr1="";
 var ehr2="";
 var ehr3="";
 //var ctx = document.getElementById('myChart').getContext('2d');
 
 
 /*$(document).ready(function () {
  var ctx = document.getElementById('myChart').getContext('2d');
  var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['BMI izbrane osebe'],
        datasets: [{
            label: 'BMI',          
            data: [0],
            backgroundColor:'rgba(54,162,235,0.6)'
        }]
    },

    // Configuration options go here
    options: {}
  });


});*/

function generirajPodatke(stPacienta) {
  var ehrId = "";

  if (stPacienta == 1) {
    var ime = "Toni";
    var priimek = "Makaroni";
    var datumRojstva = "1990-01-01"+"T00:00:00.000Z";
  } else if (stPacienta == 2) {
    var ime = "Zan";
    var priimek = "Dober";
    var datumRojstva = "1980-02-02"+"T00:00:00.000Z";
  } else if (stPacienta == 3) {
    var ime = "Andrej";
    var priimek = "Jej";
    var datumRojstva = "1970-03-03"+"T00:00:00.000Z";
  }
    
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        additionalInfo: {"ehrId": ehrId}
      };

      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
           $("#kreirajSporociloNav").html("<span class='obvestilo label label-success fade-in'>Podatki vpisani</span>");
           //$("#dodajVitalneObstojeciID")[0][stPacienta].value =ehrId;
           if(stPacienta==1){
              zapisiMeritve(ehrId, "175", "50");
              ehr1 = ehrId;
            }

            else if(stPacienta==2){
              zapisiMeritve(ehrId, "177", "70");
              ehr2 = ehrId;
            }

            else if(stPacienta==3){
              zapisiMeritve(ehrId, "160", "90");
              ehr3 = ehrId;
            }
          }
        },
        error: function(err) {
          $("#kreirajSporociloNav").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
        }
      });
    }
  })
  

  return ehrId;
}

function zapisiMeritve(ehrId, telesnaVisina, telesnaTeza) {
  
  var podatki = {
    // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": "2019-11-11T11:40Z",
      "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
      "vital_signs/body_weight/any_event/body_weight": telesnaTeza

  };
  var parametriZahteve = {
      ehrId: ehrId,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: 'jane'
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      $("#dodajMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-success fade-in'>" +
        res.meta.href + ".</span>");
    },
    error: function(err) {
      $("#dodajMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
    }
  });
}

function Ustvari() {
  var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

  if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
  var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  
  //console.log("dodajMeritve");
  //console.log(telesnaTeza);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
		// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": '2019-11-11T11:40Z',
      "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
      "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
       "vital_signs/body_temperature/any_event/temperature|magnitude": 0,
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/blood_pressure/any_event/systolic": 0,
      "vital_signs/blood_pressure/any_event/diastolic": 0,
      "vital_signs/indirect_oximetry:0/spo2|numerator": 0
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'tineBine'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}


function preberiEHRodBolnika() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
        var party = data.party;

  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Prikazana je primerna dieta za uporabnika: '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
    });

    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "weight",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        if (res.length > 0) {   
          console.log(res[0].weight);       
          ShraniTezo(res[0].weight);
        } else {
          $("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-warning fade-in'>" +
            "Ni podatkov!</span>");
        }
      },
      error: function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
    });

    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "height",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        if (res.length > 0) {   
          console.log(res[0].height);       
          ShraniVisino(res[0].height);
        } else {
          $("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-warning fade-in'>" +
            "Ni podatkov!</span>");
        }
      },
      error: function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
    });

    
    
  }
  setTimeout(function(){
    IzracunajBmi();
  },500); //delay is in milliseconds 
  
}

function preberi2(oseba) {
	if (oseba == 1){
    ehrId = ehr1
  }
  else if (oseba == 2){
    ehrId = ehr2
  }
  else if (oseba == 3){
    ehrId = ehr3
  }

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Pritisnite Generiranje podatkov");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
        var party = data.party;

  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Prikazana je primerna dieta za uporabnika: '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
    });

    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "weight",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        if (res.length > 0) {   
          console.log(res[0].weight);       
          ShraniTezo(res[0].weight);
        } else {
          $("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-warning fade-in'>" +
            "Ni podatkov!</span>");
        }
      },
      error: function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
    });

    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "height",
      type: 'GET',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        if (res.length > 0) {   
          console.log(res[0].height);       
          ShraniVisino(res[0].height);
        } else {
          $("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-warning fade-in'>" +
            "Ni podatkov!</span>");
        }
      },
      error: function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
    });

    
    
  }
  setTimeout(function(){
    IzracunajBmi();
  },500); //delay is in milliseconds 
  
}

function izvediZaUporabnike(opcija) {
  //console.log(opcija.value);
  preberi2(opcija.value);


}

function ShraniTezo(teza) {
mojaTeza = teza;
console.log(mojaTeza);
}

function ShraniVisino(visina) {
  mojaVisina = visina;
  console.log(mojaVisina);
}



function IzracunajBmi() {
  var bmi = mojaTeza/((mojaVisina/100)*(mojaVisina/100));
  
  prikazDiete(bmi);
  risiGraf(bmi);
  //chart.data.datasets[0].data = bmi;
  //chart.update();
}

function prikazDiete(bmi){
  if (bmi<18.5){
    document.getElementById("opisDiete").innerHTML = "ZAJTRK: 2 celi jajci in 3 beljaki, 115 g ovsenih kosmičev, 12g lanenega olja KOSILO: 150g piščančjih prsi, 85 g riža, 25g oreščkov(mandlji,lešniki,orehi,…) |ALI| 150g - telečje meso (ali drugo rdeče meso), 85 g riža, 12g laneno olje VEČERJA: 250g skute, solata s 85 g riža ali testenin, 12g olivnega olja";
  }

  else if (bmi > 25) {
    document.getElementById("opisDiete").innerHTML = "ZAJTRK: 1 kos polnozrnatega kruha, 2-3 rezine piščančjih prsi, lahek namaz |ALI| ovseni kosmiči z banano, 1 porcija dietnega napitka, 2 žlici skute ter 2 dcl vode. KOSILO: 100 g tune v solnici + zelena solata + paradižnik + čičerika + 1 kuhano jajce |ALI| 150 g piščanca na žaru (belo meso) pečen brez olja + brokoli + solata |ALI| 150 g lososovega fileja pečenega na žaru + brokoli + solata. VEČERJA: 150 g puste skute + malo medu (zmešaš skupaj) |ALI| 100 g tuna v solnici + paradižnik/kumarice";
  }

  else {
    document.getElementById("opisDiete").innerHTML = "ZAJTRK: Polnozrnati opečenec z džemom brez sladkorja, lahko navadni jogurt z malinami. Ob vsakem zajtrku pijte sadni ali zeliščni čaj ali kavo brez kofeina. |ALI| Nesladkani ovseni kosmiči s posnetim mlekom in jagodičjem. KOSILO: Zelenjavna juha po želji, denimo špargljeva kremna juha, zelena solata, testenine z dušeno zelenjavo, nekaj kvadratov temne čokolade. Ob vsakem kosilu vam pripada kozarec vina ali malo pivo. |ALI|  Paradižnikova juha z baziliko, zelena solata, kuhana govedina, breskve s skuto. VEČERJA:  Gobova zelenjavna juha, svinjske zarebrnice v kaprni omaki, špinača, popražena s česnom, čokoladna tortica. Po želji še kozarec vina, kakor ob vsaki večerji. |ALI| Lečna juha s slanino, zelena solata, košček polnozrnate pite ali čokoladna pena z malinami.";
  }

}
function risiGraf(bmi) {
  document.getElementById("containerId").innerHTML = '&nbsp;';
  document.getElementById("containerId").innerHTML = '<canvas id="myChart"></canvas>';
  var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create

      type: 'bar',

      // The data for our dataset
      data: {
          labels: ['BMI izbrane osebe'],
          datasets: [{
              label: 'BMI',          
              data: [bmi],
              backgroundColor:'rgba(54,162,235,0.6)'
          }]
      },

      // Configuration options go here
      
    });

    weatherAPI(function (jsonRez){
      //console.log(jsonRez.id);
      var temperatura = Math.round(jsonRez.main.temp - 273);
      
      if (temperatura > 5) {
        document.getElementById("vremenskaNapoved").innerHTML = "Trenutno je v Ljubljani " + temperatura + " stopinj, vzemite si vsaj uro časa za telovadbo v naravi!";
      }

      if (temperatura <= 5) {
        document.getElementById("vremenskaNapoved").innerHTML = "Trenutno je v Ljubljani " + temperatura + " stopinj, vzemite si vsaj uro časa za telovadbo v bližnjem fitnesu, ali doma!";
      }
      
    });
  }

  function weatherAPI(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "https://api.openweathermap.org/data/2.5/weather?id=3239318&appid=b879421bb543d57b448a13cdb6bb07c5", true);
    xobj.onreadystatechange = function () {
      // rezultat ob uspešno prebrani datoteki
      if (xobj.readyState == 4 && xobj.status == "200") {
          var json = JSON.parse(xobj.responseText);
          // vrnemo rezultat
          callback(json);
      }
    };
    
    xobj.send(null);
  }

  
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


